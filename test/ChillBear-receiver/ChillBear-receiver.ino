/*
  Simple example for receiving
  
  http://code.google.com/p/rc-switch/
  
  Need help? http://forum.ardumote.com
*/

#include <RCSwitch.h>

RCSwitch mySwitch = RCSwitch();
int OldVal,cnt;

void setup() {
  Serial.begin(115200);
  Serial.println("ChillBear Receiver....");
  mySwitch.enableReceive(0);  // GPIO 0 Receiver on interrupt 0 => that is pin D3 NodeMCU
  OldVal=0;
  pinMode(2, OUTPUT);         // on my board GPIO 2 is connected to a 433 sender module
                              // the interrupts on GPIO 0 are only working, if this GPIO is set to OUTPUT
  digitalWrite(2,0);                     
}

void loop() {
  if (mySwitch.available()) {
    
    int value = mySwitch.getReceivedValue();
    if ( value == OldVal ) {
      cnt++;
    } else {
      OldVal = value;
      cnt=0;
    }

    if (cnt == 5 ) {
      Serial.print(" GOTCHA: -------------> ");
      Serial.print(value);
      cnt=0;
    }
    
    if (value == 0) {
      Serial.print("Unknown encoding");
    } else {
      Serial.print("Received ");
      Serial.print( mySwitch.getReceivedValue() );
      Serial.print(" / ");
      Serial.print( mySwitch.getReceivedBitlength() );
      Serial.print("bit ");
      Serial.print("Protocol: ");
      Serial.println( mySwitch.getReceivedProtocol() );
    }

    mySwitch.resetAvailable();
    
  }
}
