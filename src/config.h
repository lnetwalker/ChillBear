/*
 * config file for ChillBear
 * will be later moved to a config fle on the device
*/

#ifndef CONFIG_H
#define CONFIG_H

// wifi/network settings
//#define SSID "iot-test"
//#define PASS "internet4iot"                                           
//#define SSID "pinkiebrain"
//#define PASS "a77zba_85"

#define SKETCH_VERSION "0.5.5"

#define datagramCnt 5                                               // 5 consecutive datagrams 

                                                                    // the powerplugs with their names and codes
String PowerplugNames[]   = {"AA"   ,"AB"   ,"AC"   ,"AD"   ,"AE"   ,"BA"   ,"BB"   ,"BC"   ,"BD"   ,"BE"   };
char* housecodes[]        = {"00001","00001","00001","00001","00001","00010","00010","00010","00010","00010"};
char* socketcodes[]       = {"10000","01000","00100","00010","00001","10000","01000","00100","00010","00001"};
#define plugcount 10                                                // number of elements in above arrays

char* customdevices[]     = {"Alarm","Lampe","Fun"};                // reflect changes in NumOfCustDevices below!
char* customdevicetypes[] = {"bell","lamp","fun"};
int codeProto[]           = {2,1,1};
int codeLength[]          = {32,24,24};
int codewidth[]           = {711,220,260};
#define NumOfCustDevices 3                                          // the count of the above customdevices[]

char* LEDs[]              = {"1/","2/","3/"};                       // reflect changes in NumOfLEDs below!
#define NumOfLEDs 3                                                 // amount of leds on the board

#define topiclength 50                                              // limit mqtt topics in length
#define updateInterval 60000 * 5                                    // ask every 5 Minutes for an update

// MQTT related settings
const char* mqtt_server           = "mqtt.hucky.net";
const char* mqtt_username         = "hartmut";
const char* mqtt_pass             = "p4hathc";
String mqtt_id                    = "433MHz-gateway-";
String MQTT_ID;
const uint16_t mqtt_port          = 1883;
String topic_DHT_tmpl             = "HC/ESPID/ChillBear/DHT/";
String topic_LED_tmpl             = "HC/ESPID/ChillBear/LED/";
String topic_button_tmpl          = "HC/ESPID/ChillBear/Button";
String topic_powerPlug_tmpl       = "HC/ESPID/ChillBear/POWERPLUGNAME/";
String topic_customDevice_tmpl    = "HC/ESPID/ChillBear/CUST/DEVICETYPE/NAME/";
String topic_send433_tmpl         = "HC/ESPID/ChillBear/Send433";

String subPowerplugTopics[plugcount+NumOfLEDs+NumOfCustDevices];

#define MQTTHEARTBEATTIMEOUT 30                                     // currently send heartbeat after 30 seconds

// SNMP related settings
String Firmware = "ChillBear V" + (String)SKETCH_VERSION + " ESP8266";  // build the SNMP SysDesc string with version
char* OIDsysObjectId = ".1.3.6.1.4.51063.32.0";                     // this is a must for snmp detection!
char* OIDsysDescr =  (char*)Firmware.c_str();                       // SNMP System identifier string
char* OIDsysContact = "H. Eilers <hartmut@eilers.net>";             // Contact
char* OIDsysLocation = "IoT";                                       // Location

// mDNS name resolution
const char* dnsName = "ChillBear";

// Telnet related settings

#if DEBUG_TELNET == y

#endif

#endif // CONFIG_H
