## readme for 433MQTT-transmitter

the 433MQTT-transmitter switches cheap 433MHz powerplugs on mqtt commands.

#### Setup

you need to change some settings in the sketch code to configure things:

* Set network settings: SSID,PASS
* MQTT settings: mqtt_server, mqtt_username, mqtt_pass, mqtt_port
* Set the remote control settings for powerplugs
  PowerplugNames, housecodes, socketcodes, plugcount (=number of elements in arrays )

you may set the following:
* mqtt_id
* mqtt_topic_powerPlug_tmpl ( ESPID and POWERPLUGNAME are dynamically replaced, at least 
  POWERPLUGNAME must be set )

#### Caveeats

* the length of the topics is limited to 50 characters
* any payload starting with "0" is treated as poweroff, everything else as poweron
