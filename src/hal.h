/*
 * Hardware Abstraction Layer for ChillBear
 * includes all hardware related settings
 * (c) by Hartmut Eilers <hartmut@eilers.net>
*/

#ifndef HAL_H
#define HAL_H

// 433 MHz transmitter settings
#define TRANSMITPIN 0                                               // GPIO 0 to send data
#define RECEIVEPIN 5                                                // GPIO 5 to receive data via interrupt

#define DHTPIN 4                                                    // Digital pin connected to the DHT sensor
#define SEND433 16                                                  // LED on gpio 16 to show 433 MHz activity

                                                                    // Uncomment the type of sensor in use:
//#define DHTTYPE    DHT11                                          // DHT 11
#define DHTTYPE    DHT22                                            // DHT 22 (AM2302)
//#define DHTTYPE    DHT21                                          // DHT 21 (AM2301)

// the 2 buttons on top of case
#define BUTTON1 2
#define BUTTON2 12
//#define BUTTON2 15                                                // no good choice, because the button musst be pressed to get esp up

// the attached LEDs ( Definition in setup(), control in LEDOn/LEDOff in mqtt_helper.h )
#define LED1LeftWhite  13                                           // left white LED below HOME button aka lednum=0
#define LED2LeftRed    14                                           // left red LED below HOME button aka lednum=1
#define LED3RightWhite 15                                           // right white LED below NETWORK button aka lednum=2

#endif // HAL_H