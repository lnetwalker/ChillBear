/*
 * Telnet helper functions used during telnet debugging
 *
 * (c) 2022 by Hartmut Eilers <hartmut@eilers.net>
 * distributed under the terms of the GPL V2 or later
 *
 */

#ifndef TELNET_HELPER_H
#define TELNET_HELPER_H

void Telnet()
{
  // Cleanup disconnected session

  if (TelnetClient && !TelnetClient.connected())
  {
    TelnetClient.println("disconnect Client, terminate session ");
    TelnetClient.stop();
  }
 

  // Check new client connections
  if (TelnetServer.hasClient())
  {
    
    // find free socket
    if (!TelnetClient)
    {
      TelnetClient = TelnetServer.available(); 
      
      TelnetClient.flush();  // clear input buffer, else you get strange characters
      
      TelnetClient.println("Welcome!");
    }

  }

  // check for incomming messages from telnet
  if (TelnetClient && TelnetClient.connected())
  {
    if(TelnetClient.available())
    {
      // get data from the telnet client
      while(TelnetClient.available())
      {
        #ifdef DEBUG
        Serial.print("bytes received from net: ");Serial.println(TelnetClient[i].available());
        #endif
        char received = TelnetClient.read();
      }
    }
  }
}

#endif  // TELNET_HELPER_H