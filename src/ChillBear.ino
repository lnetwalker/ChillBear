/*
 * ChillBear MQTT<->433MHz Gateway
 * 
 * This sketch connects to an mqtt broker, subscribes to topics and
 * switches 433MHz power plugs according to the mqtt messages.
 * It is also able to receive 433MHz messages from remotes and publishes 
 * the received values to a mqtt topic.
 * 
 * Besides the powerplugs the following 433 MHz devices are at least
 * partly supported:
 * - Funktürglocke FG-100 ( remote button receive works, ringing the bell doesn't)
 * - Fysic FC-50 Remote Control Lamp Unit
 * 
 * the values of an attached DHT sensor are also published over mqtt.
 * 
 * 2 Buttons and 3 LEDs on the top of the case connected to mqtt topics
 *
 * My second thought is... currently nonexistent and still forming
 * 
 * check the hal.h and config.h and configure everything there
 * 
 * (c) by Hartmut Eilers <hartmut@eilers.net>
 * released under the Terms of the GNU GPL 2.0 or later
 */

#define DEBUG_SERIAL  1                                            // use serial for debugging info
#define DEBUG_TELNET  0

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>                                           // MQTT client library
#include <RCSwitch.h>                                               // 433 MHz Remote Control library
#include <Ticker.h>                                                 // Ticker Library ( for timetriggers )
#include <Adafruit_Sensor.h>                                        // needed for DHT access
#include <DHT.h>                                                    // DHT 11/22 library
#include <WiFiUdp.h>                                                // UDP for SNMP and Telnet
#include <Arduino_SNMP.h>                                           // SNMP Library
#include <ESP8266WebServer.h>                                       // used for configuration
#include <WebConfig.h>                                              // config data library
//#include "ConfigFields.json"                                        // read the description of the configuration
#include <ESP8266mDNS.h>                                            // mDNS responder


String params = "["
"{"
"'name':'ssid',"
"'label':'WLAN name',"
"'type':"+String(INPUTTEXT)+","
"'default':''"
"},"
"{"
"'name':'pwd',"
"'label':'WLAN Password',"
"'type':"+String(INPUTPASSWORD)+","
"'default':''"
"}"
"]";

boolean WiFiApMode = false;

/*
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
*/
#include <ArduinoOTA.h>                                             // Over The Air update library

#include "hal.h"                                                    // get hardware settings
#include "config.h"                                                 // configure your settings in this file

ADC_MODE(ADC_VCC);                                                  // set internal ADC to measure board voltage

RCSwitch MQTTremote = RCSwitch();                                   // define the 433 MHz client
int OldVal,cnt;                                                     // variables used when receiving 433 MHz data

WiFiClient ChillBear;                                               // ChillBear = the Client
PubSubClient mqtt_client(ChillBear);

String ESP_Id = "";                                                 // unique identifier for every ESP, used as mqtt_id
  
Ticker MQTTheartbeat;

WiFiUDP udp;
SNMPAgent snmp = SNMPAgent("public");                               // Starts an SMMPAgent instance with the community string 'public'

DHT dht(DHTPIN, DHTTYPE);                                           // define DHT object to access dht sensor
                                                                    // DHT Sensor
float t = 0.0;                                                      // current temperature & humidity, updated in loop()
float h = 0.0;
unsigned long previousMillis, updateMillis, RemoteMillis, OldRemMillis = 0;       // will store last time things happend
const long interval =30000;                                         // Updates DHT readings every n milliseconds
const long reset433interval=5000;                                   // reset the mqtt topic for the received value after n ms
bool ResetRemote = false;                                           // if true the mqtt topic for the remote needs to be set to 0

int FreeMem,SketchMem,FreeHeap,HeapFrag,HeapMaxBlock,SysPower;      // Variables for snmp check of system health

int Button1Oldval,Button2Oldval;                                    // save the values to check for changes
int Button1Val,Button2Val;                                          // the actual value of the button         

#if DEBUG_TELNET == 1
WiFiServer TelnetServer(23);                                        // define the telnet server for debugging
WiFiClient TelnetClient;                                            // define the telnet client for debugging
#include "telnet_helper.h"                                          // telnet support functions
#endif

// where should debug messages go ?
#if DEBUG_SERIAL == 1                                               // to serial console 
#define DEBUG_PRINT(x) Serial.print(x)
#define DEBUG_PRINTLN(x) Serial.println(x)
#elif DEBUG_TELNET == 1                                             // to telnet server
#define DEBUG_PRINT(x) TelnetClient.print(x)
#define DEBUG_PRINTLN(x) TelnetClient.println(x)
#else                                                               // into the void
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#endif

#include "mqtt_helper.h"                                            // all functions related to MQTT

WebConfig conf;
ESP8266WebServer webserver;                                         // the webserver for configuration

void ConnectNetwork() {
//  DEBUG_PRINTLN("startup Wifi");
  DEBUG_PRINT("Start Wifi in ");
  if (conf.values[0] != "") {                                       // conf.values[0] is SSID
      DEBUG_PRINTLN("Station mode...");
      WiFi.setHostname(dnsName);
      WiFi.mode(WIFI_STA);                                          // try to connect as station to a network
      WiFi.begin(conf.values[0].c_str(),conf.values[1].c_str());    // conf.values[1] is passwd
      WiFi.setSleepMode(WIFI_NONE_SLEEP);
      while (WiFi.status() != WL_CONNECTED) {                       // Wait for connection
        delay(50);
        DEBUG_PRINT(".");
      }
      DEBUG_PRINT("connected to Wifi network: ");
      DEBUG_PRINTLN(conf.values[0].c_str());
      // switch on left white LED to signal that WIFI is connected
      LEDOn(0);                                                     // the left white LED
      delay(1000);
  } else {                                                          // no valid config, start accesspoint
    DEBUG_PRINTLN("WiFi AP mode ... ");
    WiFiApMode = true;
    WiFi.mode(WIFI_AP);
    DEBUG_PRINTLN(WiFi.softAP(dnsName) ? "Ready" : "Failed!");
  }
}

void handleRoot() {
  conf.handleFormRequest(&webserver);
  if (webserver.hasArg("SAVE")) {
    uint8_t cnt = conf.getCount();
    Serial.println("*********** Konfiguration ************");
    for (uint8_t i = 0; i<cnt; i++) {
      DEBUG_PRINT(conf.getName(i));
      DEBUG_PRINT(" = ");
      DEBUG_PRINTLN(conf.values[i]);
    }
    if (conf.getBool("switch")) Serial.printf("%s %s %i %5.2f \n",
                            conf.getValue("ssid"),
                            conf.getString("continent").c_str(), 
                            conf.getInt("amount"), 
                            conf.getFloat("float"));
  }
}

void setup(void) {

#ifdef DEBUG_SERIAL        
  Serial.begin(115200);                                             // full speed to monitor
#endif
  delay(1000);

  conf.setDescription(params);                                      // prepare reading config
  conf.readConfig();                                                // read the config

  DEBUG_PRINT("Verbindung zu ");
  DEBUG_PRINT(conf.values[0]);
  DEBUG_PRINTLN(" herstellen");
  ConnectNetwork();                                                 // connect to wifi
  DEBUG_PRINTLN("Network started ok");

  MDNS.setInstanceName(dnsName);                                    // start mDNS responder
  if (MDNS.begin(dnsName,WiFi.localIP())) {
    DEBUG_PRINTLN("MDNS responder gestartet");
    DEBUG_PRINTLN("http://" + String(dnsName) + ".local/");
  }

  DEBUG_PRINTLN("setup webserver handler");
  webserver.on("/",handleRoot);
  DEBUG_PRINTLN("start Webserver");
  webserver.begin(80);
  DEBUG_PRINTLN("Setup sensors and HAL");

  dht.begin();                                                      // init DHT sensor
  
  ESP_Id=ESP.getChipId();                                           // Error: getChipId returns bullshit!
  DEBUG_PRINT("ESP_Id: ");
  DEBUG_PRINTLN(ESP_Id);
  ESP_Id="359837";                                                  // dirty fix: set manually for testdevice
  DEBUG_PRINTLN("ChillBear 433MHz/MQTT Gateway");
  DEBUG_PRINT("connect as MQTT ID: ");
  DEBUG_PRINTLN(ESP_Id);

  // Button definition
  pinMode(BUTTON1,INPUT);
  pinMode(BUTTON2,INPUT);
  Button1Oldval = 1;
  Button2Oldval = 1;

  // LED definition
  pinMode(LED1LeftWhite,OUTPUT);
  pinMode(LED2LeftRed,OUTPUT);
  pinMode(LED3RightWhite,OUTPUT);
  
  // switch off the internal LED 
  digitalWrite(SEND433,HIGH);

  // signal booting: left red and right white
  LEDOff(0);
  LEDOn(1);                                                         // the red LED
  LEDOn(2);                                                         // the right white LED
  delay(1000);

  // GPIO selected as transmit pin
  pinMode(TRANSMITPIN,OUTPUT);
  MQTTremote.enableTransmit(TRANSMITPIN);
  // setup receiving part
  pinMode(RECEIVEPIN,INPUT);
  MQTTremote.enableReceive(RECEIVEPIN);

  pinMode(SEND433,OUTPUT);                                          // set LED to output

#if DEBUG_TELNET == 1
  TelnetServer.begin();
  TelnetServer.setNoDelay(true);
#endif

  DEBUG_PRINT("IP address: ");
  DEBUG_PRINTLN(WiFi.localIP()); 

  // MQTT setup
  int result = 0;                                                   // connect to MQTT broker

  if ( !WiFiApMode ) {
    while (result == 0 ){
      result = mqttConnect(mqtt_server, mqtt_port, mqtt_username, mqtt_pass);  // connect to MQTT broker
    }
  
    MQTTheartbeat.attach(MQTTHEARTBEATTIMEOUT, sendMQTTheartbeat);    // send MQTT heartbeat after that time
    DEBUG_PRINT("MQTT connect: ");
    DEBUG_PRINTLN(result);

    // setup subscriptions for the powerplugs
    for ( int i=0; i<plugcount; i++ ) {                               // generate the topicnames for the powerplugs
      String TempTopic = topic_powerPlug_tmpl;                        // replace ESPID and POWERPLUGNAME with real values
      TempTopic.replace("ESPID", ESP_Id);
      TempTopic.replace("POWERPLUGNAME", PowerplugNames[i]);
      subPowerplugTopics[i]=TempTopic;                                // store the generated topicname in subscription array
      DEBUG_PRINTLN(subPowerplugTopics[i]);
    }

    // setup subscriptions for the custom devices
    for ( int i=0; i<NumOfCustDevices; i++ ) {                        // generate the topicnames for the custom devices
      String TempCustTopic = topic_customDevice_tmpl;                 // replace ESPID and DEVICETPE and NAME with real values
      DEBUG_PRINT("TempCustTopic=");DEBUG_PRINTLN(TempCustTopic);
      TempCustTopic.replace("ESPID", ESP_Id);
      TempCustTopic.replace("NAME", customdevices[i]);
      TempCustTopic.replace("DEVICETYPE", customdevicetypes[i]);
      DEBUG_PRINT("CNT=");DEBUG_PRINTLN(i);
      DEBUG_PRINT("Name=");DEBUG_PRINTLN(customdevices[i]);
      DEBUG_PRINT("Type=");DEBUG_PRINTLN(customdevicetypes[i]);
      subPowerplugTopics[i+plugcount]=TempCustTopic;                 // store the generated topicname in subscription array
      DEBUG_PRINTLN(subPowerplugTopics[i+plugcount]);
    }

    // setup subscriptions for LEDs
    for ( int i=0; i<NumOfLEDs ; i++ ) {
      String TempLEDTopic = topic_LED_tmpl+LEDs[i];                 // generate topicname for LEDs
      DEBUG_PRINT("TempLEDTopic=");DEBUG_PRINTLN(TempLEDTopic);
      TempLEDTopic.replace("ESPID", ESP_Id);                        // replace ESPID with real value
      subPowerplugTopics[i+plugcount+NumOfCustDevices]=TempLEDTopic;// store the generated topicname in subscription array
      DEBUG_PRINTLN(subPowerplugTopics[i+plugcount+NumOfCustDevices]);
    }

    mqtt_client.setCallback (subscriptionCallback);                  // define callback function for subscriptions
  
    mqttSubTopics();                                                 // subscribe to the topics

    // SNMP setup   
    snmp.setUDP(&udp);                                               // give snmp a pointer to the UDP object
    snmp.begin();
    
    // add 'callback' for an OID - pointer to variables
    snmp.addIntegerHandler(".1.3.6.1.4.1.50163.32.1", &FreeMem);     // handler for snmp requests
    snmp.addIntegerHandler(".1.3.6.1.4.1.50163.32.2", &SketchMem);
    snmp.addIntegerHandler(".1.3.6.1.4.1.50163.32.3", &FreeHeap);
    snmp.addIntegerHandler(".1.3.6.1.4.1.50163.32.4", &HeapFrag);
    snmp.addIntegerHandler(".1.3.6.1.4.1.50163.32.5", &HeapMaxBlock);
    snmp.addIntegerHandler(".1.3.6.1.4.1.50163.32.6", &SysPower);
    snmp.addStringHandler (".1.3.6.1.2.1.1.1.0", &OIDsysDescr);
    snmp.addOIDHandler    (".1.3.6.1.2.1.1.2.0", OIDsysObjectId);
    snmp.addStringHandler (".1.3.6.1.2.1.1.4.0", &OIDsysContact);
    snmp.addStringHandler (".1.3.6.1.2.1.1.6.0", &OIDsysLocation);
    DEBUG_PRINTLN ("SNMP initialized");

  }

  // OTA Settings
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setHostname("myesp8266");

  // Simple authentication by default
  
  ArduinoOTA.setPassword((const char *)"1234");
  ArduinoOTA.setHostname(dnsName);
  
  ArduinoOTA.onStart([]() {
    #ifdef DEBUG
    Serial.println("Start");
    #endif
  });
  
  ArduinoOTA.onEnd([]() {
    #ifdef DEBUG
    Serial.println("\nEnd");
    #endif
  });
  
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    #ifdef DEBUG
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    #endif
  });
  ArduinoOTA.onError([](ota_error_t error) {
    #ifdef DEBUG
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
    #endif
  });
  
  ArduinoOTA.begin();

  // signal boot end by switching off right white LED
  LEDOff(2);
  delay(1000);
  // end off booting, switch off led0
  LEDOff(0);
}


void loop(void) {  

  #if DEBUG_TELNET == 1
  Telnet();                                                         // Handle telnet connections
  #endif

  MDNS.update();                                                    // keep mDNS working;

  webserver.handleClient();                                         // handle the webserver connections

  ArduinoOTA.handle();                    // Wait for local OTA connection

  if (!WiFiApMode)
  {

    mqtt_client.loop(); // process MQTT events

    snmp.loop(); // process snmp events
    if (snmp.setOccurred)
    {
      snmp.resetSetOccurred();
    }

    unsigned long currentMillis = millis(); // get the current milliseconds since start

    // check network connectivity
    if (WiFi.status() != WL_CONNECTED)
    { // check the network connection
      // turn on red LED only to signal WIFI connection lost
      LEDOff(0);                            // the left white LED
      LEDOn(1);                             // the red LED
      LEDOff(2);                            // the right white LED
      DEBUG_PRINT("Wifi connection lost "); // ToDo: reconnect ?
      DEBUG_PRINTLN(WiFi.status());
      ConnectNetwork();
      int result = 0; // connect to MQTT broker
      while (result == 0)
      {
        result = mqttConnect(mqtt_server, mqtt_port, mqtt_username, mqtt_pass); // connect to MQTT broker
      }
      mqttSubTopics(); // subscribe to the topics
    }

    // receiving 433MHz signal from remote
    if (MQTTremote.available())
    { // we received something from a remote

      int value = MQTTremote.getReceivedValue(); // read the value from the remote
      if (value == OldVal)
      {        // the same value received again
        cnt++; // count it
      }
      else
      {                 // we read a different value from the remote
        OldVal = value; // save it and restart counting
        cnt = 0;
      }

      if (cnt == datagramCnt)
      { // found n consecutive datagrams with the same value, so post it via mqtt
        DEBUG_PRINT(" GOTCHA: -------------> ");
        DEBUG_PRINTLN(value);
        cnt = 0;
        String TempTopic = topic_powerPlug_tmpl; // replace ESPID with real value
        TempTopic.replace("ESPID", ESP_Id);
        TempTopic.replace("/POWERPLUGNAME/", "/"); // remove the POWERPLUGNAME from topic
        TempTopic = TempTopic + "received";
        char PubTempTopic[topiclength]; // prepare the topic for sending
        TempTopic.toCharArray(PubTempTopic, TempTopic.length() + 1);
        char DatenChar[4];                                  // convert the value to string
        sendmqtt(PubTempTopic, itoa(value, DatenChar, 10)); // publish via mqtt
        previousMillis = RemoteMillis;                      // schedule reset the value
      }
      if (value == 0)
      { // just for debugging, show what we have received
        DEBUG_PRINT("Unknown encoding");
      }
      else
      {
        DEBUG_PRINT("Received ");
        DEBUG_PRINT(MQTTremote.getReceivedValue());
        DEBUG_PRINT(" / ");
        DEBUG_PRINT(MQTTremote.getReceivedBitlength());
        DEBUG_PRINT("bit ");
        DEBUG_PRINT("Protocol: ");
        DEBUG_PRINT(MQTTremote.getReceivedProtocol());
        DEBUG_PRINT(" delay: ");
        DEBUG_PRINTLN(MQTTremote.getReceivedDelay());
      }
      MQTTremote.resetAvailable();
      ResetRemote = true;
    }

    // DHT Sensor
    currentMillis = millis();
    if (currentMillis - previousMillis >= interval)
    {                                     // check DHT sensor for changes
      previousMillis = currentMillis;     // save the last time you updated the DHT value
      float newT = dht.readTemperature(); // Read temperature as Celsius (the default)
      //float newT = dht.readTemperature(true);                       // Read temperature as Fahrenheit (isFahrenheit = true)
      if (isnan(newT))
      { // if temperature read failed, don't change t value
        DEBUG_PRINTLN("Failed to read temperature from DHT sensor!");
      }
      else
      {
        t = newT;
        DEBUG_PRINT("Temp=");
        DEBUG_PRINTLN(t);
      }
      float newH = dht.readHumidity(); // Read Humidity
      if (isnan(newH))
      { // if humidity read failed, don't change h value
        DEBUG_PRINTLN("Failed to read humidity from DHT sensor!");
      }
      else
      {
        h = newH;
        DEBUG_PRINT("Hum=");
        DEBUG_PRINTLN(h);
      }
      publishDHT2MQTT(t, h);
    }

    // ocasionally do random stuff like cleaning states
    RemoteMillis = millis();
    if (RemoteMillis - OldRemMillis >= reset433interval)
    {
      OldRemMillis = RemoteMillis;
      if (ResetRemote)
      {                                          // reset the 433MHz received value by sending 0
        String TempTopic = topic_powerPlug_tmpl; // replace ESPID with real value
        TempTopic.replace("ESPID", ESP_Id);
        TempTopic.replace("/POWERPLUGNAME/", "/"); // remove the POWERPLUGNAME from topic
        TempTopic = TempTopic + "received";
        char PubTempTopic[topiclength]; // prepare the topic for sending
        TempTopic.toCharArray(PubTempTopic, TempTopic.length() + 1);
        char DatenChar[4];                              // convert the value to string
        sendmqtt(PubTempTopic, itoa(0, DatenChar, 10)); // publish via mqtt
        ResetRemote = false;
      }

      for (int i = 0; i < NumOfLEDs; i++) // send the status of the LEDs via MQTT
      {
        String TempTopic = topic_LED_tmpl + LEDs[i] + "currentState"; // publish current state on topic .../currentState
        TempTopic.replace("ESPID", ESP_Id);
        char PubTempTopic[TempTopic.length() + 1];
        TempTopic.toCharArray(PubTempTopic, TempTopic.length() + 1);
        if (LEDState(i) == HIGH)
        {
          sendmqtt(PubTempTopic, "1");
          DEBUG_PRINT(" high state of LED: ");
          DEBUG_PRINTLN(i + 1);
        }
        else
        {
          sendmqtt(PubTempTopic, "0");
          DEBUG_PRINT(" low state of LED: ");
          DEBUG_PRINTLN(i + 1);
        }
      }

      // collect snmp data
      FreeMem = ESP.getFreeSketchSpace();       // free sketch memory
      SketchMem = ESP.getSketchSize();          // sketch size
      FreeHeap = ESP.getFreeHeap();             // free Heap size
      HeapFrag = ESP.getHeapFragmentation();    // % heap Fragmentation  (0% is clean, more than ~50% is not harmless)
      HeapMaxBlock = ESP.getMaxFreeBlockSize(); // size of the biggest free block in heap
      /* 
     * FIXME
     * as soon as I enable ESP.getVCC() I get WiFi disconnect errors
     * the wifi disconnects after some seconds ( 3-6 s ), restarts
     * without problems fast, but again disconnects
    SysPower = ESP.getVcc();                                          // CPU voltage
     *
    int VoltageCPU = ESP.getVcc();                                    // CPU voltage
    DEBUG_PRINT("CPU-Voltage: ");
    DEBUG_PRINTLN(VoltageCPU);
     * if I enable above test everything works
     */
    }

    // Button Handling
    Button1Val = digitalRead(BUTTON1);
    Button2Val = digitalRead(BUTTON2);
    if (Button1Val != Button1Oldval)
    {
      // the value for the button has changed, update via MQTT
      DEBUG_PRINT("BUTTON1=");
      DEBUG_PRINTLN(Button1Val);
      String ButtonTopic = topic_button_tmpl; // replace ESPID with real value
      DEBUG_PRINTLN(ButtonTopic);
      ButtonTopic.replace("ESPID", ESP_Id);
      ButtonTopic = ButtonTopic + "1";
      char PubTempTopic[topiclength]; // prepare the topic for sending
      ButtonTopic.toCharArray(PubTempTopic, ButtonTopic.length() + 1);
      char DatenChar[4];                                       // convert the value to string
      sendmqtt(PubTempTopic, itoa(Button1Val, DatenChar, 10)); // publish via mqtt
      Button1Oldval = Button1Val;                              // update the old val to check for next change
    }
    if (Button2Val != Button2Oldval)
    {
      // the value for the button has changed, update via MQTT
      DEBUG_PRINT("BUTTON2=");
      DEBUG_PRINTLN(Button2Val);
      String ButtonTopic = topic_button_tmpl; // replace ESPID with real value
      ButtonTopic.replace("ESPID", ESP_Id);
      ButtonTopic = ButtonTopic + "2";
      char PubTempTopic[topiclength]; // prepare the topic for sending
      ButtonTopic.toCharArray(PubTempTopic, ButtonTopic.length() + 1);
      char DatenChar[4];                                       // convert the value to string
      sendmqtt(PubTempTopic, itoa(Button2Val, DatenChar, 10)); // publish via mqtt
      Button2Oldval = Button2Val;                              // update the old val to check for next change
    }
  }
}