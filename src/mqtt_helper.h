/*
 * all MQTT related helper functions 
*/

#ifndef MQTT_HELPER_H
#define MQTT_HELPER_H



void LEDOn(int lednum) {
  /* 
   * this function switches the LED lednum (0-2) on
  */
  switch (lednum) {
    case 0: 
      digitalWrite(LED1LeftWhite,HIGH);
      break;
    case 1:  
      digitalWrite(LED2LeftRed,HIGH);
      break;
    case 2:
      digitalWrite(LED3RightWhite,HIGH);
      break;
  }
}

void LEDOff(int lednum) {
  /* 
   * this function switches the LED lednum (0-2) off
  */
  switch (lednum) {
    case 0: 
      digitalWrite(LED1LeftWhite,LOW);
      break;
    case 1:  
      digitalWrite(LED2LeftRed,LOW);
      break;
    case 2:
      digitalWrite(LED3RightWhite,LOW);
      break;
  }
}

int LEDState(int lednum) {
  /* 
   * this function reads the LED lednum (0-2) state
  */
 int val;
  switch (lednum) {
    case 0: 
      val = digitalRead(LED1LeftWhite);
      break;
    case 1:  
      val = digitalRead(LED2LeftRed);
      break;
    case 2:
      val = digitalRead(LED3RightWhite);
      break;
  }
  return val;
}

void mqttSubTopics() {                                              // subscribe to topics for each powerplug
/*
 * subscribe to MQTT topics
 * all topics in the subscription array are subscribed
 */
  for ( int i=0; i<plugcount+NumOfCustDevices+NumOfLEDs; i++ ) {
    String TempTopic = subPowerplugTopics[i] + "wantedState";
    DEBUG_PRINTLN(TempTopic);
    char SubTempTopic[topiclength];
    TempTopic.toCharArray(SubTempTopic,TempTopic.length()+1);       // WTF why +1 ?
    mqtt_client.subscribe(SubTempTopic); 
    DEBUG_PRINT ("MQTT Sub: ");
    DEBUG_PRINTLN(SubTempTopic);
  }
}


                                                                    // connects to a MQTT broker, returns 0 on success, 1 on error
boolean mqttConnect(const char* my_mqtt_server, uint16_t my_mqtt_port, const char* my_mqtt_username, const char* my_mqtt_pass){

  mqtt_client.setClient(ChillBear);
  mqtt_client.setServer(my_mqtt_server, my_mqtt_port);
  // apend the unique ID of the ESP to the mqtt_id to ensure unique ID
  MQTT_ID=mqtt_id + ESP_Id;
  boolean state=mqtt_client.connect(MQTT_ID.c_str(),my_mqtt_username,my_mqtt_pass);
  if ( state ) {
    DEBUG_PRINTLN("connected to mqtt");
    // signal success  by switch off red led
    LEDOff(1);                                                      // the red LED
    delay(1000);
    return true;
  } else {
    DEBUG_PRINTLN("MQTT connection Error");
    DEBUG_PRINTLN(mqtt_client.state());
    return false;
  }
  
}


boolean sendmqtt(char* topic,char *value) {                         // send a value to topic on mqtt broker
  
  boolean success = false;
  while ( !success ) {
    DEBUG_PRINT("publishing to topic: ");
    DEBUG_PRINTLN(topic);
    boolean state=mqtt_client.publish(topic, value, true);          // publish the converted value ( retained )
    if ( !state ) {                                                 // on error reconnect to MQTT broker
      DEBUG_PRINT (mqtt_client.state());
      DEBUG_PRINTLN(" MQTT publish Error, reconnecting");
      state=mqttConnect(mqtt_server, mqtt_port, mqtt_username, mqtt_pass); 
      if ( state ) {                                                // on reconnect - resubscribe to the needed topics
        mqttSubTopics();                          
        DEBUG_PRINTLN(" ...connected");
      }
    } else {
      success = true;                                              // successful published to topic
    }
  }
  return success;
}


                                                                    // switch a powerplug on and reflect the state via MQTT
void powerplugOn(int ppnum) {
  digitalWrite(SEND433,LOW);
  MQTTremote.switchOn(housecodes[ppnum], socketcodes[ppnum]);       // switch on the plug
  String TempTopic;                                                 // publish current state on topic .../currentState
  TempTopic = subPowerplugTopics[ppnum] + "currentState";
  char PubTempTopic[topiclength];
  TempTopic.toCharArray(PubTempTopic,TempTopic.length()+1);
  sendmqtt (PubTempTopic,"1");
  DEBUG_PRINT("switched pLUG On: ");DEBUG_PRINTLN(ppnum);
  digitalWrite(SEND433,HIGH);
}


                                                                    // switch a powerplug off and reflect the state via MQTT
void powerplugOff(int ppnum) {
  digitalWrite(SEND433,LOW);
  MQTTremote.switchOff(housecodes[ppnum], socketcodes[ppnum]);      // switch off the plug

  String TempTopic;                                                 // publish current state on topic .../currentState
  TempTopic = subPowerplugTopics[ppnum] + "currentState";
  char PubTempTopic[topiclength];
  TempTopic.toCharArray(PubTempTopic,TempTopic.length()+1);
  sendmqtt (PubTempTopic,"0");
  DEBUG_PRINT("switched pLUG Off: ");DEBUG_PRINTLN(ppnum);
  digitalWrite(SEND433,HIGH);
}


void publishDHT2MQTT(float temp,float hum) {                        // publish the DHT sensor readings to MQTT
  String DHTTempTopic = topic_DHT_tmpl;                             // get the topic template
  DEBUG_PRINT("puplish DHT data to : ");
  DEBUG_PRINTLN(DHTTempTopic);
  DHTTempTopic.replace("ESPID", ESP_Id);                            // replace ESPID with real value
  String DHTHumTopic = DHTTempTopic + "hum";                        // gen topics for temp and hum
  DHTTempTopic = DHTTempTopic + "temp";
  char DHTPubTempTopic[topiclength];                                // buffer for the topic
  DHTTempTopic.toCharArray(DHTPubTempTopic,DHTTempTopic.length()+1);// write topic to buffer
  char DHTTempStr[8];                                               // buffer for the temperature
  dtostrf(temp, 6, 2, DHTTempStr);                                  // fill buffer with  the float value
  sendmqtt (DHTPubTempTopic,DHTTempStr);                            // publish temp
  DHTHumTopic.toCharArray(DHTPubTempTopic,DHTHumTopic.length()+1);  // repeat the same for humidity
  char DHTHumStr[8];
  dtostrf(hum, 6, 2, DHTHumStr);
  sendmqtt (DHTPubTempTopic,DHTHumStr);
  DEBUG_PRINTLN("temp + hum send over MQTT");
}


void sendMQTTheartbeat () {                                         // send a heartbeat signal over MQTT
  String HeartbeatTopic = "";
  HeartbeatTopic = topic_powerPlug_tmpl;
  DEBUG_PRINT("topic_powerPlug_tmpl=");
  DEBUG_PRINTLN(topic_powerPlug_tmpl);
  HeartbeatTopic.replace("ESPID", ESP_Id);                          // replace ESPID with real value
  HeartbeatTopic.replace("/POWERPLUGNAME/", "/");                   // throw away the powerplugname
  HeartbeatTopic = HeartbeatTopic + "alive";
  DEBUG_PRINT("HeartBeatTopic=");
  DEBUG_PRINTLN(HeartbeatTopic);
  char PubTempTopic[topiclength];
  HeartbeatTopic.toCharArray(PubTempTopic,HeartbeatTopic.length()+1);
  sendmqtt (PubTempTopic,"1");
  MQTTheartbeat.attach(MQTTHEARTBEATTIMEOUT, sendMQTTheartbeat);    // restart the timer
  DEBUG_PRINTLN("heartbeat send over MQTT");
}


                                                                    // callback function is called every time a subscribed topic is received
void subscriptionCallback(char* topic, byte* payload, unsigned int length) {
                                                                    // we received a topic, now check for which powerplug it is
  int i=0;
  boolean found=false;

  DEBUG_PRINT(" rec Topic ");
  DEBUG_PRINTLN(topic);
                                                                    // check wether the received topic is one of the powerplugs
  while ( (i < plugcount) && (found==false) ) {                     // loop over the topics to get the number of the received topic
    String TempTopic = subPowerplugTopics[i]+"wantedState";
    //DEBUG_PRINTLN(TempTopic);
    if ( strcmp(topic,TempTopic.c_str()) == 0 ) {                   // found topic
      if ( payload[0] == 48 ) {                                     // ASCII Code for "0"
        powerplugOff(i);                                            // switch off plug
      } else {
        powerplugOn(i);                                             // switch on plug
      }
      found=true;
    }
    i++;                                                            // check next topic
  }
  if (!found) {                                                     // it was none of the powerplugs, check custom devices
    String CurrentTopic = String(topic);
    String ReceivedPayload = String((char*)payload).substring(0,length); // convert the payload to a string                                 //
    int j=0;
    while ( ( j<NumOfCustDevices ) && (found==false) ) {                           // check through the custom topics for a match
      String DeviceTypeSearch=customdevicetypes[j];
      int TypePosition = CurrentTopic.indexOf(DeviceTypeSearch);
      DEBUG_PRINTLN(CurrentTopic);
      DEBUG_PRINTLN(DeviceTypeSearch);
      DEBUG_PRINTLN(TypePosition);
      if ( TypePosition > 0 ) {                                     // it's really one of the custom topics
        DEBUG_PRINT("found custom topic, just send received payload=");
        DEBUG_PRINTLN(ReceivedPayload);       
        found=true;
        digitalWrite(SEND433,LOW);
        unsigned long code = ReceivedPayload.toInt();
        DEBUG_PRINTLN(j);
        DEBUG_PRINTLN(codeProto[j]);
        DEBUG_PRINTLN(codewidth[j]);
        DEBUG_PRINTLN(codeLength[j]);
        MQTTremote.setProtocol(codeProto[j],codewidth[j]);          // send the received code with the 
        MQTTremote.setRepeatTransmit(15); 
        MQTTremote.send(code,codeLength[j]);                        // needed protocoldata
        digitalWrite(SEND433,HIGH);
      }
      j++;
    }
  }
  i = 0;                                                            // initialize counter for LED
  while ( (!found) && ( i<NumOfLEDs ) ) {                           // check the LED topics
    DEBUG_PRINT("scan for LED topic: ");
    DEBUG_PRINT("TopicTemplate=");
    DEBUG_PRINTLN(topic_LED_tmpl);
    DEBUG_PRINT("LEDs=");
    DEBUG_PRINTLN(LEDs[i]);
    String LEDTopic = topic_LED_tmpl + LEDs[i] + "wantedState";
    LEDTopic.replace("ESPID", ESP_Id);
    DEBUG_PRINT("LEDTopic.c_str=");
    DEBUG_PRINTLN(LEDTopic.c_str());
    if ( strcmp(topic,LEDTopic.c_str()) == 0 ) {                    // found topic
      DEBUG_PRINTLN("Found!");
      found=true;

      String TempTopic = topic_LED_tmpl + LEDs[i] + "currentState"; // publish current state on topic .../currentState
      TempTopic.replace("ESPID", ESP_Id);
      char PubTempTopic[TempTopic.length()+1];
      TempTopic.toCharArray(PubTempTopic,TempTopic.length()+1);

      if ( payload[0] == 48 ) {                                     // ASCII Code for "0"
        LEDOff(i);                                                  // switch off LED
        sendmqtt (PubTempTopic,"0");
        DEBUG_PRINT(" low state of LED: ");DEBUG_PRINTLN(i+1);
      } else {
        LEDOn(i);                                                   // switch on LED
        sendmqtt (PubTempTopic,"1");
        DEBUG_PRINT(" high state of LED: ");DEBUG_PRINTLN(i+1);
      }
    }
    i++;
  }
  if ( !found ) {                                                   // nothing matches return an error
    DEBUG_PRINTLN("ERROR: received a topic with no subscription!!");
    DEBUG_PRINT("Topic: ");DEBUG_PRINTLN(topic);
  }
}

#endif // MQTT_HELPER_H