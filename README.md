# ChillBear

## Version 0.5.1
ChillBear is a MQTT to 433 MHz Gateway with enhanced Features.

ChillBear can be used to control cheap 433MHz PowerPlugs over MQTT. Can be
used in e.g. Home-Assistant in connection with the MQTT extension.

The following features are available:

- configurable amount of powerplugs
- DHT 22 sensor with temperature and humidity
- 3 LEDs for status information
- 2 buttons as event triggers
- SNMP support
- custom 433 MHz devices


### MQTT information
In the topics the following values are replaced before sending data:
|Variable|Comment|
|---------------|---------------------------------------------------------|
|ESPID | replaced by the ESPid value of the ESP 8266|
|POWERPLUGNAME | will be replaced with the configured names of the plugs|
|NAME | replaced by the name of the custom device|
|DEVICTETYPE | replaced with the type of the custom device|
|n | ( 1-3 ) number of the LED|
|m | ( 1-2 ) number of the button|

The following MQTT Topics are available ( see config.h for configuration ):
|Topic|Type|
|---------------------------------------------------|------------------------|
|HC/ESPID/ChillBear/POWERPLUGNAME/wantedState       | subscribed by device|
|HC/ESPID/ChillBear/POWERPLUGNAME/currentState      | pub by dev|
|HC/ESPID/ChillBear/DHT/temp                        | pub by dev|
|HC/ESPID/ChillBear/DHT/hum                         | pub by dev|
|HC/ESPID/ChillBear/LED/n/wantedState               | sub by dev|
|HC/ESPID/ChillBear/LED/n/currentState              | pub by dev|
|HC/ESPID/ChillBear/Button/m                        | pub by dev|
|HC/ESPID/ChillBear/CUST/DEVICETYPE/NAME/           | sub by dev|
|HC/ESPID/ChillBear/Send433/wantedValue             | sub by dev|
|HC/ESPID/ChillBear/Send433/currentValue            | pub by dev|
|HC/ESPID/ChillBear/received                        | pub by dev|
|HC/ESPID/ChillBear/alive                           | pub by dev|

### SNMP info
the following data is published via SNMP

|OID|Value|Comment|
|------------------------|----------------|---------------------------------------------------------------------------|
|.1.3.6.1.4.1.50163.32.1 | &FreeMem | free sketch memory |
|.1.3.6.1.4.1.50163.32.2 | &SketchMem | sketch size |
|.1.3.6.1.4.1.50163.32.3 | &FreeHeap | free Heap size |
|.1.3.6.1.4.1.50163.32.4 | &HeapFrag | % heap Fragmentation  (0% is clean, more than ~50% is not harmless) |
|.1.3.6.1.4.1.50163.32.5 | &HeapMaxBlock | size of the biggest free block in heap |
|.1.3.6.1.4.1.50163.32.6 | &SysPower | CPU voltage |
|.1.3.6.1.2.1.1.1.0 | &OIDsysDescr | description set in config.h |
|.1.3.6.1.2.1.1.2.0 | OIDsysObjectId | .1.3.6.1.4.51063.32.0 |
|.1.3.6.1.2.1.1.4.0 | &OIDsysContact | contact for device, set in config.h |
|.1.3.6.1.2.1.1.6.0 | &OIDsysLocation | device location, set in config.h |

### Setup

open config.h and change everything according to your needs, compile the code and upload to device

### Hardware

I used a nodemcu, see hal.h for the wiring,
schematics and description are updated later on

